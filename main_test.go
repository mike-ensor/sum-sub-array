package main

import (
	"fmt"
	"strings"
	"testing"
)

func TestMainMethod(t *testing.T) {
	initial := []int{1, -1, 3, 0, -1, 1, 3, 5, 7, 9} // Array[2:10] = 27
	expected := 27
	result := getSumsFromList(initial)

	PrintResultArr(result)

	max := result[len(result)-1].sum
	if max != expected {
		t.Errorf("Result max %d did not match expected max %d", max, expected)
	}
}

func TestMainMethodWithStrangeArray(t *testing.T) {
	initial := []int{1, -7, 9, 6, 4} // 19 [2:4]
	expected := 19
	result := getSumsFromList(initial)

	PrintResultArr(result)

	max := result[len(result)-1].sum
	if max != expected {
		t.Errorf("Result max %d did not match expected max %d", max, expected)
	}
}

func TestMainMethodWithAllNegative(t *testing.T) {
	initial := []int{-1, -7, -9, -6, -4} // single instance of -1 to be max
	expected := -1
	result := getSumsFromList(initial)

	PrintResultArr(result)

	max := result[len(result)-1].sum
	if max != expected {
		t.Errorf("Result max %d did not match expected max %d", max, expected)
	}
}

func TestMainMethodWithRandomList(t *testing.T) {
	initial := GenerateInitialArrayList(5)
	str := strings.Trim(strings.Join(strings.Fields(fmt.Sprint(initial)), ","), "[]")
	t.Logf("Initial array: %v", str)
	result := getSumsFromList(initial)

	PrintResultArr(result)

	if len(result) != 15 {
		t.Errorf("Results should be sized 15, found %d", len(result))
	}

	max := result[len(result)-1].sum

	t.Logf("Max: %d", max)
}

func TestInsertItemInArray(t *testing.T) {
	number := 7
	initial := []Result{{1, 0, 1}, {3, 0, 1}, {5, 0, 1}, {10, 0, 1}}

	expected := []Result{{1, 0, 1}, {3, 0, 1}, {5, 0, 1}, {number, 0, 1}, {10, 0, 1}}

	result := AddToSortedArray(Result{number, 0, 1}, initial)

	if !arrayContentsEqual(expected, result) {
		t.Errorf("Arrays were not equal")
		PrintResultArr(result)
	}
}

func TestInsertItemInArrayAtEnd(t *testing.T) {
	number := 11
	initial := []Result{{1, 0, 1}, {3, 0, 1}}

	expected := []Result{{1, 0, 1}, {3, 0, 1}, {number, 0, 1}}

	result := AddToSortedArray(Result{number, 0, 1}, initial)

	if !arrayContentsEqual(expected, result) {
		t.Errorf("Arrays were not equal")
		PrintResultArr(result)
	}
}

func TestPrintResultArr(t *testing.T) {
	initial := []Result{{2, 0, 3}, {4, 1, 6}}

	PrintResultArr(initial)
}

func arrayContentsEqual(base []Result, compare []Result) bool {

	if len(base) != len(compare) {
		return false // cannot possibly be equal if they have different sizes
	}

	isEqual := true
	for index, item := range base {
		isEqual = (item.sum == compare[index].sum)
		if !isEqual {
			break
		}
	}

	return isEqual
}
