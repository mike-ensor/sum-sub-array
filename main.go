package main

import (
	"fmt"
	"math/rand"
	"time"
)

var initialArraySize int = 40
var maxRandomIntegerSize int = 2000
var outputFieldPadding int = 7

// Result holds the sum of a sequence and the start/end
type Result struct {
	sum   int
	start int
	end   int
}

func main() {
	list := GenerateInitialArrayList(initialArraySize)
	fmt.Print("Initial Array: ")
	Print(list)

	result := getSumsFromList(list)

	max := result[len(result)-1]
	fmt.Printf("Largest Sum: %3d, over subset [%d:%d]", max.sum, max.start, max.end)
}

func getSumsFromList(list []int) []Result {
	result := []Result{}
	listIndexEnd := len(list)

	for start := 0; start < listIndexEnd; start++ {
		firstRun := true
		sum := 0
		for end := listIndexEnd; end > start; end-- {
			// optimize to reduce the number of array runs, just take the start:end sum and remove the :end value
			if firstRun {
				sum = GetSum(start, list[start:end])
				firstRun = false
			} else {
				sum = sum - list[end]
			}
			// fmt.Printf("Array[%d:%d] = %d\n", start, end, sum)
			resultObj := Result{sum: sum, start: start, end: end - 1} // zero vs 1 index
			result = AddToSortedArray(resultObj, result)
		}
		firstRun = true
	}

	return result
}

// AddToSortedArray Adds to an array
func AddToSortedArray(number Result, initialArray []Result) []Result {
	// iterate through pre-sorted array, if number >= initialArray[i] (insert), else continue
	result := []Result{}
	initialArrLength := len(initialArray)
	// base-case (0)
	if initialArrLength == 0 {
		arr := CreateResultArray(1)
		arr[0] = number
		return arr // Ugh, I don't like multiple return statements
	}

	// End edge case. Since sorted array, if array's max number is less than incoming number, just append
	currentMax := initialArray[initialArrLength-1]
	if currentMax.sum <= number.sum {
		result = append(initialArray, number)
		return result
	}

	// insert into existing array to keep sorted
	for index, item := range initialArray {
		if item.sum >= number.sum {
			// fmt.Printf("\tInsert %d\n", number)
			left := initialArray[0:index]
			result = CreateResultArray(index)
			copy(result, left)
			result = append(result, number)

			// only add the "right" side if there is a right-side
			if index < initialArrLength {
				right := initialArray[index:]
				result = append(result, right...)
			}
			break // no need to continue, insert has been completed
		}
	}

	return result
}

// CreateArray to create a new array of size int
func CreateArray(size int) []int {
	return make([]int, size)
}

// CreateResultArray creates a new array of Result objects
func CreateResultArray(size int) []Result {
	return make([]Result, size)
}

// GetSum returns the sum of all digits in the array/slice
func GetSum(start int, initialArray []int) int {
	sum := 0
	for _, item := range initialArray {
		sum += item
	}

	return sum
}

// GenerateInitialArrayList creates an array of integers
func GenerateInitialArrayList(size int) []int {
	rand.Seed(time.Now().UnixNano())
	list := CreateArray(size)
	for i := 0; i < size; i++ {
		number := rand.Intn(maxRandomIntegerSize)
		// Attempt to make some sort of random negative numbers
		if rand.Intn(99)%2 == 0 && i%2 == 1 {
			number = number * -1
		}
		list[i] = number
	}

	return list
}

// Print prints value to out
func Print(arr []int) {
	for _, value := range arr {
		stringFormat := fmt.Sprintf("%%%-dv", outputFieldPadding)
		fmt.Printf(stringFormat, value)
	}
	fmt.Print("\n")
}

// PrintResultArr prints out the Results array sum
func PrintResultArr(resultArr []Result) {
	for _, value := range resultArr {
		fmt.Printf("Sum %d = range[%d:%d]\n", value.sum, value.start, value.end)
	}
}
