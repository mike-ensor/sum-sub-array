# Overview

This is a simple (not overly complex) solution to an interview-style problem.

# Problem

Given an array of integers (neg and positive) what is the largest sum of a sequence. For example:  1, 3, 5, -2 would be 9 (1+3+5)

# Solution
>Note: this is **my** solution and may not be the most optimal (built this in a hour). It's not O(n^3), but it could be improved.

1. Iterate the original input from 0..n as index (start->end)
1. For each index, sum from that point to the end of the array and save the sum + index start/ending (coordinates)
1. Bump the index by 1 and repeat step #2
1. For each sum in #2, save the sum in an array that is sorted producing a sorted array of the lowest to highest sum sequences

# Run

```bash
go run main.go
```